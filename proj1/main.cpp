//CS3600, Project 1
//Section A, FS16
//October 4, 2016
//Geoffrey Cline
//Nick Sallwasser


#include <iostream>

using namespace std;

//given two integers, returns the GCD
//a&b cannot both be zero
int myGCD(int a, int b);

//recursive function to find linear combination
//a & b cannot both be zero
int myLinComb(int a, int b, int *x, int *y);

int main() {
	
    int ia, ib;
	int x, y;

    cout << "Enter an integer value for a:" << endl;
    cin >> ia;
    cout << "Enter an integer value for b:" << endl;
    cin >> ib;
	
	int g = myGCD(ia,ib);
    cout << "The GCD of a and b is: " << g << endl;
	
	myLinComb(ia, ib, &x, &y);

	cout<<"The linear combination of GCD(a,b) is: " <<ia
		<<"*"<<x <<" + "<<ib <<"*"<<y <<" = "<< g<<endl<<endl;

    return 0;
}

int myGCD(int a, int b)
{
    int r;

    if(a < 0) a = -a;
    if(b < 0) b = -b;

    if(a == 0 && b == 0)
        r = -1;
    else if(b == 0)
        r = a;
    else
        r = myGCD(b, a % b);

    return r;
}

int myLinComb(int ia, int ib, int *x, int *y)
{
	int xnew, ynew;

    //Base Case  
    if (ia == 0)
    {
        *x = 0;
        *y = 1;
        return ib;
    }
 
    //Recursive Call with updated a & b
    myLinComb(ib%ia, ia, &xnew, &ynew);
 
    //update x & y recursive values
    *x = ynew - (ib/ia) * xnew;
    *y = xnew;
 
    return 0;
}