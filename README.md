# cs3600-p1
## Header
CS3600, Section A, FS 16

October 4, 2016

Geoffrey Cline  and  Nick Sallwasser

## Compile
a CMakeLists.txt file is provided for use with CMake, and then Make

`cmake proj1`

`make`

`./cs3600_p1`